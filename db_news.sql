-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 03, 2020 at 02:24 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_news`
--

-- --------------------------------------------------------

--
-- Table structure for table `komen`
--

CREATE TABLE `komen` (
  `id_komen` int(11) NOT NULL,
  `berita_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `komen` varchar(255) NOT NULL,
  `tanggal_komen` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `komen`
--

INSERT INTO `komen` (`id_komen`, `berita_id`, `nama`, `email`, `komen`, `tanggal_komen`) VALUES
(1, 28, 'zuhad', 'zuhad.shoesmart@gmail.com', 'ini percobaan komen', '2019-12-19'),
(2, 28, 'danang', 'danang.shoesmart@gmail.com', 'dsdsdsdsds', '2019-12-20'),
(3, 28, 'danang', 'danang.shoesmart@gmail.com', 'ewfrdsafs', '2019-12-20'),
(4, 28, 'zuhad', 'zuhad.shoesmart@gmail.com', 'percobaan komen lagi', '2019-12-20'),
(5, 28, 'zuhad', 'zuhad.shoesmart@gmail.com', 'percobaan komen lagi', '2019-12-20'),
(6, 28, 'zuhad', 'zuhad.shoesmart@gmail.com', 'komen lagi', '2019-12-20'),
(7, 28, 'zuhad', 'zuhad.shoesmart@gmail.com', 'komen lagi 2', '2019-12-20'),
(8, 27, 'zuhad', 'zuhad.shoesmart@gmail.com', 'komen lagi untuk percobaan', '2019-12-20'),
(9, 36, 'zuhad', 'zuhad.shoesmart@gmail.com', 'toppp', '2019-12-26'),
(10, 36, 'dad', 'dadada@gmail.com', 'dsgsdg', '2020-01-02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_berita`
--

CREATE TABLE `tbl_berita` (
  `berita_id` int(11) NOT NULL,
  `berita_judul` varchar(150) DEFAULT NULL,
  `berita_isi` text DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `berita_image` varchar(40) DEFAULT NULL,
  `berita_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `tags` mediumtext NOT NULL,
  `kategori` mediumtext NOT NULL,
  `kontributor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_berita`
--

INSERT INTO `tbl_berita` (`berita_id`, `berita_judul`, `berita_isi`, `link`, `berita_image`, `berita_tanggal`, `tags`, `kategori`, `kontributor`) VALUES
(36, 'Sejarah Shoesmart', '<p>Cerita bermula pada 1985 ketika Bapak Antonio Tanuwijaya mendirikan pabrik sepatu home industry di kota Surabaya. Bersama ratusan pegawai, ia memproduksi ribuan pasang alas kaki yang telah mendukung langkah banyak orang dalam melakukan aktivitas dan pekerjaannya. Tiga dekade berlalu, Bapak Antonio kemudian menyerahkan tongkat estafet bisnisnya itu kepada Andrew Daniel Tanuwijaya, putra bungsunya. Andrew pun segera bergerak cepat membawa bisnis kebanggaan sang Ayah itu ke ranah digital.</p>\r\n\r\n<p>Maka di tahun 2016, ia mendirikan Shoesmart sebagai footwear marketplace: pusat belanja aneka macam sepatu secara online. Shoesmart menaungi beragam merk sepatu dalam negeri. Melalui Shoesmart, Andrew menghadirkan konsep baru, praktis, dan mudah bagi masyarakat dalam berbelanja sepatu.</p>\r\n\r\n<p>Hingga saat ini, beberapa brand sepatu lokal telah bergabung di Shoesmart. Modelnya bermacam-macam menyesuaikan tren perkembangan fashion. Menjalankan bisnis ini, Andrew terus memegang teguh komitmen sang Ayah untuk mensejahterakan para pengrajin sepatu lokal dan UMKM di Indonesia.</p>\r\n\r\n<p>Dengan begitu, menurut Andrew, roda perekonomian bangsa akan terus berputar. Vendor-vendor di Indonesia lantas tidak miskin proyek, brand-brand sepatu lokal selalu banjir order, pabrik bahan bisa terus memproduksi, dan masyarakat pun mendapat lapangan pekerjaan yang layak.</p>\r\n', NULL, 'afb8a1a40e5d13f8f328e4141f86bd65.jpg', '2019-12-24 09:35:56', 'sejarah,shoesmart,sepatu,sepatu lokal, ukm indonesia,produk lokal,sepatu surabaya', '4', 1),
(42, 'ini berita baru lo', '<p>ini berita baru sekali hangat</p>\r\n', NULL, '64661313a6ca5ad2b4399014d8cada55.jpg', '2020-01-02 09:04:50', 'coba,sejarah', '4', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `keterangan_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `nama_kategori`, `keterangan_kategori`) VALUES
(1, 'hijab', 'hijab fashion'),
(2, 'Sport', 'sport lifestyle'),
(4, 'Sejarah Perusahaan', 'Sejarah Shoesmart');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kontributor`
--

CREATE TABLE `tbl_kontributor` (
  `id_kontributor` int(11) NOT NULL,
  `nama_kontributor` varchar(50) NOT NULL,
  `username_kontributor` varchar(20) DEFAULT NULL,
  `email_kontributor` varchar(50) NOT NULL,
  `telp_kontributor` varchar(13) NOT NULL,
  `about_kontributor` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kontributor`
--

INSERT INTO `tbl_kontributor` (`id_kontributor`, `nama_kontributor`, `username_kontributor`, `email_kontributor`, `telp_kontributor`, `about_kontributor`) VALUES
(1, 'zuhad achmad fachrian', 'zfachrian', 'zuhad.shoesmart@gmail.com', '082736475831', 'Thank you for stopped by, I’m a Creative Content Writer and writing about productivity, lifestyle, fashion and skin care.'),
(2, 'danang', NULL, 'danang.shoesmart@gmail.com', '089764536273', '<p>saya adalah penulis laskar pelangi dan Gundala putra petir</p>\r\n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `komen`
--
ALTER TABLE `komen`
  ADD PRIMARY KEY (`id_komen`);

--
-- Indexes for table `tbl_berita`
--
ALTER TABLE `tbl_berita`
  ADD PRIMARY KEY (`berita_id`),
  ADD KEY `fk_kontributor` (`kontributor`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tbl_kontributor`
--
ALTER TABLE `tbl_kontributor`
  ADD PRIMARY KEY (`id_kontributor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `komen`
--
ALTER TABLE `komen`
  MODIFY `id_komen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_berita`
--
ALTER TABLE `tbl_berita`
  MODIFY `berita_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_kontributor`
--
ALTER TABLE `tbl_kontributor`
  MODIFY `id_kontributor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_berita`
--
ALTER TABLE `tbl_berita`
  ADD CONSTRAINT `fk_kontributor` FOREIGN KEY (`kontributor`) REFERENCES `tbl_kontributor` (`id_kontributor`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
