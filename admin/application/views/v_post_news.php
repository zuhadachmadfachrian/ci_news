<!DOCTYPE html>
<html>

<head>
	<title>Post blog</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/bootstrap.css' ?>">
	<link rel="stylesheet" href="<?php echo base_url() . 'assets/css/bootstrap-tagsinput.css' ?>">
</head>

<body>

	<div class="container">
		<div class="col-md-8 col-md-offset-2">
			<h2 class="text-center">POST ARTIKEL</h2>
			<hr />
			<form action="<?php echo base_url() . 'index.php/post_berita/simpan_post' ?>" method="post" enctype="multipart/form-data">
				<input type="text" name="judul" class="form-control" placeholder="Judul berita" required /><br />
				<textarea id="ckeditor" name="berita" class="form-control" required></textarea><br />
				<input type="text" id="tags" name="tags" data-role="tagsinput" placeholder="tags berita"> pisahkan dengan koma tanpa spasi
				<br><br>
				<input type="file" name="filefoto" required><br>
				<div class="form-group row">
					<div class="col-sm-12"> Nama Kontributor :
						<select class="form-control" id="kontributor" name="kontributor">
							<?php
							foreach ($kontributor as $kontri) {
							?>
								<option value="<?= $kontri['id_kontributor']; ?>"><?= $kontri['nama_kontributor']; ?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-12"> Kategori Berita :
						<div class="checkbox form-control">
							<?php
							foreach ($kategori as $kat) {
							?>
								<label>
									<input type="checkbox" name="options[]" value="<?= $kat['id_kategori']; ?>"><?= $kat['nama_kategori']; ?>
								</label>
							<?php
							}
							?>
						</div>
					</div>
				</div>
				<input type="hidden" name="kate" class="form-control" placeholder="kategori" value="" required />

				<button class="btn btn-primary" type="submit">Post Berita</button>
				<a href="<?php echo base_url() . 'index.php/post_berita/lists' ?>" class="btn btn-success">List Berita</a>
			</form>
		</div>
	</div>

	<script src="<?php echo base_url() . 'assets/jquery/jquery-2.2.3.min.js' ?>"></script>
	<script type="text/javascript" src="<?php echo base_url() . 'assets/js/bootstrap.js' ?>"></script>
	<script src="<?php echo base_url() . 'assets/ckeditor/ckeditor.js' ?>"></script>
	<script type="text/javascript">
		$(function() {
			CKEDITOR.replace('ckeditor');
		});
	</script>
	<script src="<?php echo base_url() . 'assets/js/bootstrap-tagsinput.min.js' ?>"></script>
	<script>
		$(function() {
			$('input, select').on('change', function(event) {
				var $element = $(event.target),
					$container = $element.closest('.example');

				if (!$element.data('tagsinput'))
					return;

				var val = $element.val();
				if (val === null)
					val = "null";
				$('code', $('pre.val', $container)).html(($.isArray(val) ? JSON.stringify(val) : "\"" + val.replace('"', '\\"') + "\""));
				$('code', $('pre.items', $container)).html(JSON.stringify($element.tagsinput('items')));
			}).trigger('change');
		});
	</script>
	<script>
		function calculate() {

			var arr = $.map($('input:checkbox:checked'), function(e, i) {
				return +e.value;
			});

			$('input[name=kate]').val(arr.join(','));

		}

		calculate();

		$('div').delegate('input:checkbox', 'click', calculate);
	</script>
</body>

</html>