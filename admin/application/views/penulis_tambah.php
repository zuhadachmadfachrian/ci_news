<!DOCTYPE html>
<html>

<head>
    <title>Post blog</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/bootstrap.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/bootstrap-tagsinput.css' ?>">
</head>

<body>

    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <h2 class="text-center">Tambah Data Penulis</h2>
            <hr />
            <form action="<?php echo base_url() . 'index.php/post_berita/tambahPenulis' ?>" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <div class="col-sm-12"> Nama Penulis :
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nama" required />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12"> Tentang Penulis :
                        <textarea id="ckeditor" name="about" id="about" class="form-control" required></textarea>
                    </div>
                </div>
                <!-- <div class="form-group row">
                    <div class="col-sm-12"> Foto Penulis :
                        <input type="file" name="filefoto" required>
                    </div>
                </div> -->
                <div class="form-group row">
                    <div class="col-sm-12"> Telp Penulis :
                        <input type="number" name="telp" id="telp" class="form-control" placeholder="Nomor Telepon" required />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12"> Email Penulis :
                        <input type="email" name="email" id="email" class="form-control" placeholder="email" required />
                    </div>
                </div>
                <br>
                <button class="btn btn-primary" type="submit">Tambah Penulis</button>
                <a href="<?php echo base_url() . 'index.php/post_berita/penulis' ?>" class="btn btn-success">Kembali</a>
            </form>
        </div>
    </div>

    <script src="<?php echo base_url() . 'assets/jquery/jquery-2.2.3.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/js/bootstrap.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/ckeditor/ckeditor.js' ?>"></script>
    <script type="text/javascript">
        $(function() {
            CKEDITOR.replace('ckeditor');
        });
    </script>
</body>

</html>