<!DOCTYPE html>
<html>

<head>
    <title>Post blog</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/bootstrap.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/bootstrap-tagsinput.css' ?>">
</head>

<body>

    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <h2 class="text-center">Tambah Data Kategori</h2>
            <hr />
            <form action="<?php echo base_url() . 'index.php/post_berita/tambahKategori' ?>" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <div class="col-sm-12"> Nama Kategori :
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nama" required />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12"> keterangan kategori :
                        <input type="teks" name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan" required />
                    </div>
                </div>
                <br>
                <button class="btn btn-primary" type="submit">Tambah Kategori</button>
                <a href="<?php echo base_url() . 'index.php/post_berita/kategori' ?>" class="btn btn-success">kembali</a>
            </form>
        </div>
    </div>


    <script src="<?php echo base_url() . 'assets/jquery/jquery-2.2.3.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/js/bootstrap.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/ckeditor/ckeditor.js' ?>"></script>
    <script type="text/javascript">
        $(function() {
            CKEDITOR.replace('ckeditor');
        });
    </script>
</body>

</html>