<h1 class="text-center mt-4">Product List</h1>

<table class="table table-striped table-bordered">

  <?php
  if ($this->session->flashdata('success')) {
  ?>
    <div class="alert alert-success text-center" style="margin-top:20px;">
      <?php echo $this->session->flashdata('success'); ?>
    </div>
  <?php
  } ?>
  <?php
  if ($this->session->flashdata('edit')) {
  ?>
    <div class="alert alert-success text-center" style="margin-top:20px;">
      <?php echo $this->session->flashdata('edit'); ?>
    </div>
  <?php
  } ?>
  <?php
  if ($this->session->flashdata('hapus')) {
  ?>
    <div class="alert alert-danger text-center" style="margin-top:20px;">
      <?php echo $this->session->flashdata('hapus'); ?>
    </div>
  <?php
  } ?>

  <thead>
    <tr>
      <th width="50" style="text-align:center;">#</th>
      <th scope="col">Judul</th>
      <th width="200">Isi Berita</th>
      <th width="400" style="text-align:center;">Action</th>
    </tr>
  </thead>
  <?php
  $no = 1;
  function limit_words($string, $word_limit)
  {
    $words = explode(" ", $string);
    return implode(" ", array_splice($words, 0, $word_limit));
  }
  foreach ($data->result_array() as $i) :
    $id = $i['berita_id'];
    $judul = $i['berita_judul'];
    $image = $i['berita_image'];
    $isi = $i['berita_isi'];
  ?>
    <tr>
      <td style="text-align:center;"><?= $no++; ?></td>
      <td><?= $i['berita_judul']; ?></td>
      <!-- <td><img style="width: 100px;" src="<?php echo base_url() . '../assets/images/' . $image; ?>"></td> -->
      <td><?= $i['berita_judul']; ?></td>
      <td>
        <!-- <a href="<?= site_url('post_berita/view/' . $id); ?>" class="btn btn-sm btn-warning">View</a> -->
        <a href="<?= site_url('post_berita/updateArtikel/' . $id); ?>" class="btn btn-sm btn-info">Update</a>
        <a href="<?= site_url('post_berita/delete/' . $id); ?>" class="btn btn-sm btn-danger">Delete</a>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
</div>
<div>
  <center>
    <button id="singlebutton" name="singlebutton" class="btn btn-success center-block">
      <a href="<?= base_url() . 'index.php/post_berita/tambahArtikel' ?>" class="btn btn-success text-center">Post Artikel</a>
    </button>
  </center>
</div>