<h1 class="text-center mt-4">List Penulis</h1>

<table class="table table-striped table-bordered">

    <?php
    if ($this->session->flashdata('success')) {
    ?>
        <div class="alert alert-success text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php
    } ?>
    <?php
    if ($this->session->flashdata('edit')) {
    ?>
        <div class="alert alert-success text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('edit'); ?>
        </div>
    <?php
    } ?>
    <?php
    if ($this->session->flashdata('hapus')) {
    ?>
        <div class="alert alert-danger text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('hapus'); ?>
        </div>
    <?php
    } ?>

    <thead>
        <tr>
            <th width="50" style="text-align:center;">#</th>
            <th scope="col">Nama</th>
            <th width="200">About</th>
            <th width="200">email</th>
            <th width="200">Telp</th>
            <th width="200" style="text-align:center;">Action</th>
        </tr>
    </thead>
    <?php
    $no = 1;
    function limit_words($string, $word_limit)
    {
        $words = explode(" ", $string);
        return implode(" ", array_splice($words, 0, $word_limit));
    }
    foreach ($penulis as $pen) :

    ?>
        <tr>
            <td style="text-align:center;"><?= $no++; ?></td>
            <td><?= $pen['nama_kontributor']; ?></td>
            <td><?= $pen['about_kontributor']; ?></td>
            <td><?= $pen['email_kontributor']; ?></td>
            <td><?= $pen['telp_kontributor']; ?></td>
            <td>
                <!-- <a href="<?= site_url('post_berita/view/' . $pen['id_kontributor']); ?>" class="btn btn-sm btn-warning">View</a> -->
                <a href="<?= site_url('post_berita/updatePenulis/' . $pen['id_kontributor']); ?>" class="btn btn-sm btn-info">Update</a>
                <a href="<?= site_url('post_berita/deletePenulis/' . $pen['id_kontributor']); ?>" class="btn btn-sm btn-danger">Delete</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
<div class="form-group">
    <label class="col-md-6 control-label" for="singlebutton"></label>
    <div class="col-md-6">
        <button id="singlebutton" name="singlebutton" class="btn btn-success center-block">
            <a href="<?= base_url() . 'index.php/post_berita/tambahPenulis' ?>" class="btn btn-success text-center">Tambah Penulis</a>
        </button>
    </div>
</div>