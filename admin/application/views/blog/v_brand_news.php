<!DOCTYPE html>
<html>
<head>
	<title>Post blog</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
</head>
<body>
	
	<div class="container">
		<div class="col-md-8 col-md-offset-2">
			<h2 class="text-center">Tambah Brand</h2><hr/>
			<form action="<?php echo base_url().'index.php/post_brand/simpan_post'?>" method="post" enctype="multipart/form-data">
	            <input type="text" name="brand" class="form-control" placeholder="nama brand" required/><br/>
	            <input type="file" name="filefoto" required><br>
	            <button class="btn btn-primary" type="submit">Post Berita</button>
				<a href="<?php echo base_url().'index.php/brand/lists'?>" class="btn btn-success">List Berita</a>
            </form>
		</div>
	</div>
	
	
	<script src="<?php echo base_url().'assets/jquery/jquery-2.2.3.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
	<script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
	<script type="text/javascript">
	  $(function () {
	    CKEDITOR.replace('ckeditor');
	  });
	</script>
</body>
</html>