<h1 class="text-center mt-4">List Kategori</h1>

<table class="table table-striped table-bordered">

    <?php
    if ($this->session->flashdata('success')) {
    ?>
        <div class="alert alert-success text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php
    } ?>
    <?php
    if ($this->session->flashdata('edit')) {
    ?>
        <div class="alert alert-success text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('edit'); ?>
        </div>
    <?php
    } ?>
    <?php
    if ($this->session->flashdata('hapus')) {
    ?>
        <div class="alert alert-danger text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('hapus'); ?>
        </div>
    <?php
    } ?>

    <thead>
        <tr>
            <th width="50" style="text-align:center;">#</th>
            <th scope="col">Nama Kategori</th>
            <th width="200">Keterangan</th>
            <th width="200" style="text-align:center;">Action</th>
        </tr>
    </thead>
    <?php
    $no = 1;
    foreach ($kategori as $kat) {
    ?>
        <tr>
            <td style="text-align:center;"><?= $no++; ?></td>
            <td><?= $kat['nama_kategori']; ?></td>
            <td><?= $kat['keterangan_kategori']; ?></td>
            <td>
                <a href="<?= site_url('post_berita/updateKategori/' . $kat['id_kategori']); ?>" class="btn btn-sm btn-info">Update</a>
                <a href="<?= site_url('post_berita/deleteKategori/' . $kat['id_kategori']); ?>" class="btn btn-sm btn-danger">Delete</a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
</div>
<div class="form-group">
    <label class="col-md-6 control-label" for="singlebutton"></label>
    <div class="col-md-6">
        <button id="singlebutton" name="singlebutton" class="btn btn-success center-block">
            <a href="<?= base_url() . 'index.php/post_berita/tambahKategori' ?>" class="btn btn-success text-center">Tambah Kategori</a>
        </button>
    </div>
</div>