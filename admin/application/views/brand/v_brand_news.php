<!DOCTYPE html>
<html>
<head>
	<title>Post blog</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
</head>
<body>
	
	<div class="container">
		<div class="col-md-3 col-md-offset-4">
			<h2 class="text-center">Tambah Brand</h2><hr/>
			<form action="<?php echo base_url().'index.php/brand/simpan_post'?>" method="post" enctype="multipart/form-data">
	            <input type="file" name="filefoto" class="form-control" required><br>
				<input type="text" name="keterangan" class="form-control" placeholder="keterangan" required/><br/>
	            <button class="btn btn-primary" type="submit">tambah</button>
				<a href="<?php echo base_url().'index.php/brand/lists'?>" class="btn btn-success">List Brand</a>
            </form>
		</div>
	</div>
	
	
	<script src="<?php echo base_url().'assets/jquery/jquery-2.2.3.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
	<script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
	<script type="text/javascript">
	  $(function () {
	    CKEDITOR.replace('ckeditor');
	  });
	</script>
</body>
</html>