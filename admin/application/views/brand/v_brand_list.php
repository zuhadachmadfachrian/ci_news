<!DOCTYPE html>
<html>
<head>
	<title>Post blog</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
	<link href="<?php echo base_url().'assets/css/jquery.datatables.min.css'?>" rel="stylesheet" type="text/css"/>
  	<link href="<?php echo base_url().'assets/css/dataTables.bootstrap.css'?>" rel="stylesheet" type="text/css"/>
</head>
<body>
	
	<div class="container">
		<?php
			function limit_words($string, $word_limit){
                $words = explode(" ",$string);
                return implode(" ",array_splice($words,0,$word_limit));
            }
			foreach ($data->result_array() as $i) :
				$id=$i['id_brand'];
				$image=$i['gambar'];
				$keterangan=$i['keterangan'];
		?>
		<div class="col-md-8 col-md-offset-2">
			<h2><?php echo $keterangan;?></h2><hr/>
			<img src="<?php echo base_url().'../assets/images/brand/'.$image;?>">
			<?php echo limit_words($isi,30);?><a href="<?php echo base_url().'index.php/post_berita/view/'.$id;?>"> Selengkapnya ></a>
		</div>
		<?php endforeach;?>
	</div>



	<div class="container">
        <h1 class="text-center mt-4">Product List</h1>
      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">nama</th>
            <th width="200">logo</th>
            <th width="200">Action</th>
          </tr>
        </thead>
        <?php
          $count = 0;
          foreach ($data->result_array() as $row) :
            $id         =$row['id_brand'];
            $image      =$row['gambar'];
            $keterangan =$row['keterangan'];
            $count++;
        ?>
          <tr>
            <td scope="row"><?= $count;?></td>
            <td><?= $row->keterangan;?></td>
            <td><img style="width: 100px;" src="<?php echo base_url().'../assets/images/brand/'.$row->images;?>"></td>
            <td>
              <a href="<?= site_url('product/get_edit/'.$row->id_product);?>" class="btn btn-sm btn-info">Update</a>
              <a href="<?= site_url('product/delete/'.$row->id_product);?>" class="btn btn-sm btn-danger">Delete</a>
            </td>
          </tr>
        <?php endforeach;?>
        </tbody>
      </table>
    </div>

	
	<script src="<?php echo base_url().'assets/jquery/jquery-2.2.3.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
	<script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
	<script src="<?php echo base_url().'assets/js/jquery-2.1.4.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/js/jquery.datatables.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/js/dataTables.bootstrap.js'?>"></script>
	<script type="text/javascript">
	  $(function () {
	    CKEDITOR.replace('ckeditor');
	  });
	</script>
</body>
</html>	
	