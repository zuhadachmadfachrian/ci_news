<?php
class M_berita extends CI_Model
{

	//penulis/author
	function getAllPenulis()
	{
		return $this->db->query("SELECT * FROM tbl_kontributor ORDER BY id_kontributor DESC")->result_array();
	}

	function getPenulisByID($id)
	{
		return $this->db->get_where('tbl_kontributor', ['id_kontributor' => $id])->row_array();
	}

	function getAllKontributorByID($id) //penulis join 
	{
		return $this->db->query("
			select a.*, b.* from tbl_berita as a join tbl_kontributor as b ON a.kontributor = b.id_kontributor where a.berita_id=$id;
		")->result_array();
	}

	public function tambahDataPenulis()
	{
		$data = [
			"nama_kontributor" => $this->input->post("name"),
			"email_kontributor" => $this->input->post("email"),
			"telp_kontributor" => $this->input->post("telp"),
			"about_kontributor" => $this->input->post("about"),
		];

		$this->db->insert("tbl_kontributor", $data);
	}

	public function updateDataPenulis($id)
	{
		$data = [
			"nama_kontributor" => $this->input->post("name"),
			"email_kontributor" => $this->input->post("email"),
			"telp_kontributor" => $this->input->post("telp"),
			"about_kontributor" => $this->input->post("about"),
		];

		$this->db->where('id_kontributor', $id);
		$this->db->update("tbl_kontributor", $data);
	}

	public function deletePenulisByID($id)
	{
		$this->db->delete('tbl_kontributor', ['id_kontributor' => $id]);
	}

	function simpan_berita($jdl, $berita, $gambar, $tags, $kontributor, $kategori)
	{
		$hsl = $this->db->query("INSERT INTO tbl_berita (berita_judul,berita_isi,berita_image, tags, kontributor, kategori) VALUES ('$jdl','$berita','$gambar', '$tags', '$kontributor', '$kategori')");
		return $hsl;
	}


	//berita
	function get_berita_by_kode($kode)
	{
		$hsl = $this->db->query("SELECT * FROM tbl_berita WHERE berita_id='$kode'");
		return $hsl;
	}

	function get_all_berita()
	{
		$hsl = $this->db->query("SELECT * FROM tbl_berita ORDER BY berita_id DESC");
		return $hsl;
	}
	public function delete_berita($id)
	{
		$this->db->delete('tbl_berita', ['berita_id' => $id]);
	}

	public function getAllberitaByID($id)
	{
		return $this->db->get_where('tbl_berita', ['berita_id' => $id])->row_array();
	}

	public function editDataberita($gambar)
	{
		$data = [
			"berita_judul" => $this->input->post("judul"),
			"berita_isi" => $this->input->post("berita"),
			"berita_image" => $gambar,
			// "berita_tanggal" => $this->input->post("berita"),
			"tags" => $this->input->post("tags"),
			"kategori" => $this->input->post("kate"), // Store foods with comma separate 
			"kontributor" => $this->input->post("kontributor"),
		];

		$this->db->where('berita_id', $this->input->post('idBer'));
		$this->db->update("tbl_berita", $data);
	}

	public function editBerita() //no image
	{
		$data = [
			"berita_judul" => $this->input->post("judul"),
			"berita_isi" => $this->input->post("berita"),
			// "berita_image" => $gambar,
			// "berita_tanggal" => $this->input->post("berita"),
			"tags" => $this->input->post("tags"),
			"kategori" => $this->input->post("kate"),
			"kontributor" => $this->input->post("kontributor"),
		];

		$this->db->where('berita_id', $this->input->post('idBer'));
		$this->db->update("tbl_berita", $data);
	}


	//kategori
	function getAllKategori()
	{
		return $this->db->get('tbl_kategori')->result_array();
	}

	function getKategoriByID($id)
	{
		return $this->db->get_where('tbl_kategori', ['id_kategori' => $id])->row_array();
	}

	function getAllKategoriJoin($id) //kategori join 
	{
		return $this->db->query("
			select a.*, b.* from tbl_berita as a join tbl_kategori as b ON a.kategori = b.id_kategori where a.berita_id=$id;
		")->row_array();
	}

	public function tambahDataKategori()
	{
		$data = [
			"nama_kategori" => $this->input->post("name"),
			"keterangan_kategori" => $this->input->post("keterangan"),
		];

		$this->db->insert("tbl_kategori", $data);
	}

	public function updateDataKategori($id)
	{
		$data = [
			"nama_kategori" => $this->input->post("name"),
			"keterangan_kategori" => $this->input->post("keterangan"),
		];

		$this->db->where('id_kategori', $id);
		$this->db->update("tbl_kategori", $data);
	}

	public function deleteKategoriByID($id)
	{
		$this->db->delete('tbl_kategori', ['id_kategori' => $id]);
	}
}
