<?php
class Post_berita extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_berita');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		// $this->load->library('dbutil');
		$this->load->library('form_validation');
		$this->load->helper('file');
	}
	function index()
	{
		$x['data'] = $this->m_berita->get_all_berita();
		$this->load->view('templates/header');
		$this->load->view('v_post_list', $x);
		$this->load->view('templates/footer');
	}

	// kategori

	function kategori()
	{
		$x['kategori'] = $this->m_berita->getAllKategori();
		$this->load->view('templates/header');
		$this->load->view('kategori_list', $x);
		$this->load->view('templates/footer');
	}

	public function tambahKategori()
	{
		$this->form_validation->set_rules("name", "name", "required");
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('kategori_tambah');
		} else {
			$this->load->model("M_berita");
			$this->m_berita->tambahDataKategori();
			$this->session->set_flashdata('success', 'Data Berhasil ditambah');
			redirect("post_berita/kategori");
		}
	}

	public function updateKategori($id)
	{
		$this->form_validation->set_rules("name", "name", "required");
		if ($this->form_validation->run() == FALSE) {
			$data['kat'] = $this->m_berita->getKategoriByID($id);
			$this->load->view('kategori_update', $data);
		} else {
			$this->load->model("M_berita");
			$this->m_berita->updateDataKategori($id);
			$this->session->set_flashdata('success', 'Data Berhasil ditambah');
			redirect("post_berita/penulis");
		}
	}

	public function deleteKategori($id)
	{
		$this->load->model("M_berita");
		$this->m_berita->deleteKategoriByID($id);
		$this->session->set_flashdata('hapus', 'Data Berhasil dihapus');

		redirect('post_berita');
	}

	// penulis

	function penulis()
	{
		$x['penulis'] = $this->m_berita->getAllPenulis();
		$this->load->view('templates/header');
		$this->load->view('list_penulis', $x);
		$this->load->view('templates/footer');
	}

	public function tambahPenulis()
	{
		$this->form_validation->set_rules("name", "name", "required");
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('penulis_tambah');
		} else {
			$this->load->model("M_berita");
			$this->m_berita->tambahDataPenulis();
			$this->session->set_flashdata('success', 'Data Berhasil ditambah');
			redirect("post_berita/penulis");
		}
	}

	public function updatePenulis($id)
	{
		$this->form_validation->set_rules("name", "name", "required");
		if ($this->form_validation->run() == FALSE) {
			$data['pen'] = $this->m_berita->getPenulisByID($id);
			$this->load->view('penulis_update', $data);
		} else {
			$this->load->model("M_berita");
			$this->m_berita->updateDataPenulis($id);
			$this->session->set_flashdata('success', 'Data Berhasil ditambah');
			redirect("post_berita/penulis");
		}
	}

	public function deletePenulis($id)
	{
		$this->load->model("M_berita");
		$this->m_berita->deletePenulisByID($id);
		$this->session->set_flashdata('hapus', 'Data Berhasil dihapus');

		redirect('post_berita');
	}

	// artikel

	function tambahArtikel()
	{
		$x['kontributor'] = $this->m_berita->getAllPenulis();
		$x['kategori'] = $this->m_berita->getAllKategori();
		$this->load->view('v_post_news', $x);
	}

	function simpan_post()
	{
		$config['upload_path'] = '../assets/images/'; //path folder
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

		$this->upload->initialize($config);
		if (!empty($_FILES['filefoto']['name'])) {
			if ($this->upload->do_upload('filefoto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/images/' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['quality'] = '60%';
				$config['width'] = 710;
				$config['height'] = 420;
				$config['new_image'] = './assets/images/' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$gambar = $gbr['file_name'];
				$jdl = $this->input->post('judul');
				$berita = $this->input->post('berita');
				$tags = $this->input->post("tags");
				$kontributor = $this->input->post("kontributor");
				$kategori = $this->input->post('kate'); // Store foods with comma separate 

				$this->m_berita->simpan_berita($jdl, $berita, $gambar, $tags, $kontributor, $kategori);
				redirect('post_berita/lists');
			} else {
				redirect('post_berita');
			}
		} else {
			redirect('post_berita');
		}
	}

	function lists()
	{
		$x['data'] = $this->m_berita->get_all_berita();
		$this->load->view('templates/header');
		$this->load->view('v_post_list', $x);
		$this->load->view('templates/footer');
	}

	function view()
	{
		$kode = $this->uri->segment(3);
		$x['data'] = $this->m_berita->get_berita_by_kode($kode);
		$this->load->view('v_post_view', $x);
	}

	public function updateArtikel($id)
	{
		$this->form_validation->set_rules("judul", "judul", "required");
		if ($this->form_validation->run() == FALSE) {
			$this->load->model("M_berita");
			$data["berita"] = $this->m_berita->getAllberitaByID($id);
			$data["kontributor"] = $this->m_berita->getAllKontributorByID($id);
			$data["kontrib"] = $this->m_berita->getAllPenulis();
			$data["kategori"] = $this->m_berita->getAllKategoriJoin($id);
			$data["kate"] = $this->m_berita->getAllKategori();
			$this->load->view('v_post_news_update', $data);
		} else {

			$config['upload_path'] = '../assets/images/'; //path folder
			$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
			$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

			$this->upload->initialize($config);
			if (!empty($_FILES['filefoto']['name'])) {
				if ($this->upload->do_upload('filefoto')) {
					$gbr = $this->upload->data();
					//Compress Image
					$config['image_library'] = 'gd2';
					$config['source_image'] = './assets/images/' . $gbr['file_name'];
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = FALSE;
					$config['quality'] = '60%';
					$config['width'] = 710;
					$config['height'] = 420;
					$config['new_image'] = './assets/images/' . $gbr['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$gambar = $gbr['file_name'];

					$this->load->model("M_berita");
					$this->m_berita->editDataberita($gambar);
					$this->session->set_flashdata('edit', 'Data Berhasil diubah');
					redirect("post_berita");
				}
			} else {
				$this->load->model("M_berita");
				$this->m_berita->editBerita();
				$this->session->set_flashdata('edit', 'Data Berhasil diubah');
				redirect("post_berita");
			}
		}
	}

	public function delete($id)
	{
		$this->load->model("M_berita");
		$this->m_berita->delete_berita($id);
		$this->session->set_flashdata('hapus', 'Data Berhasil dihapus');

		redirect('post_berita');
	}
}
