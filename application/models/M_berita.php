<?php
class M_berita extends CI_Model
{

	// berita-----------------------------

	function simpan_berita($jdl, $berita, $gambar)
	{
		$hsl = $this->db->query("INSERT INTO tbl_berita (berita_judul,berita_isi,berita_image) VALUES ('$jdl','$berita','$gambar')");
		return $hsl;
	}

	public function getAllberitaByID($kode)
	{
		return $this->db->get_where('tbl_berita', ['berita_id' => $kode])->row_array();
	}

	function get_all_berita()
	{
		$hsl = $this->db->query("SELECT * FROM tbl_berita ORDER BY berita_id DESC");
		return $hsl;
	}

	// tag-----------------------------

	function getAllTagBy($id)
	{
		return $this->db->query("
			SELECT * FROM tbl_berita WHERE tags LIKE '%$id%'
		")->result_array();
	}

	// kategori-----------------------------
	function getAllKategori()
	{
		return $this->db->get('tbl_kategori')->result_array();
	}

	function getAllKategoriByID($kode)
	{
		return $this->db->query("
			
			select a.*, b.* from tbl_berita as a join tbl_kategori as b on a.kategori = b.id_kategori 
			where a.berita_id = $kode;
		")->result_array();
	}

	function cariKategori($id)
	{
		return $this->db->query("
			
			select a.*, b.* from tbl_berita as a join tbl_kategori as b on a.kategori = b.id_kategori 
			where a.kategori LIKE '%$id%';
		")->result_array();
	}

	function GetKategori($id)
	{
		return $this->db->get_where('tbl_kategori', ['id_kategori' => $id])->row_array();
	}

	// kontributor-----------------------------
	function getAllKontributoriByID($kode)
	{
		return $this->db->query("
			
			select a.*, b.* from tbl_berita as a join tbl_kontributor as b on a.kontributor = b.id_kontributor 
			where a.berita_id = $kode;
		")->result_array();
	}

	// komen-----------------------------

	function getAllKomenByID($kode)
	{
		return $this->db->get_where('komen', ['berita_id' => $kode])->result_array();
	}

	function tambahKomen()
	{
		$data = [
			"berita_id" => $this->input->post("berita_id"),
			"nama" => $this->input->post("name"),
			"email" => $this->input->post("email"),
			"komen" => $this->input->post("komen"),
			"tanggal_komen" => $this->input->post("tgl"),
		];

		$this->db->insert("komen", $data);
	}
}
