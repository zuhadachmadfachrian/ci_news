<?php
$b = $data;
?>
<div class="templateux-section" style="background-image: url(<?php echo base_url() . 'assets/images/aa.jpg' ?>);">
  <div class="container">
    <div class="row align-items-center">

    </div>
  </div>
</div> <!-- .templateux-cover -->

<div id="blog" class="templateux-section">
  <div class="container">
    <!-- <iframe width="100%" height="70%" src="https://www.youtube.com/embed/UuLNCO4S4KY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
    <img width="100%" src="<?php echo base_url() . 'assets/images/' . $b['berita_image']; ?>">
    <div class="row mt-5">
      <div class="col-md-8">
        <h2 class="mb-3"><?php echo $b['berita_judul']; ?></h2>

        <!-- <p><img src="images/img_7.jpg" alt="" class="img-fluid"></p> -->

        <?php echo $b['berita_isi']; ?>

        <div class="tag-widget post-tag-container mb-5 mt-5">
          <div class="tagcloud">
            <?php
            $tg = explode(',', $b['tags']);
            // print_r($tg);
            foreach ($tg as $t) {
            ?>
              <a href="<?= base_url() . 'index.php/post_berita/cariTags/' . $t; ?>" class="tag-cloud-link"><?= $t; ?></a>
            <?php
            }
            ?>
          </div>
        </div>

        <?php
        if ($this->session->flashdata('success')) {
        ?>
          <div class="alert alert-success text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php
        } ?>
        <?php
        if ($this->session->flashdata('edit')) {
        ?>
          <div class="alert alert-success text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('edit'); ?>
          </div>
        <?php
        } ?>
        <?php
        if ($this->session->flashdata('hapus')) {
        ?>
          <div class="alert alert-danger text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('hapus'); ?>
          </div>
        <?php
        } ?>
        <div class="pt-5 mt-5">
          <h3 class="mb-5"> Comment</h3>
          <ul class="comment-list">
            <?php
            foreach ($komen as $kom) {
            ?>
              <li class="comment">
                <div class="vcard bio">
                  <img src="<?php echo base_url() . 'assets/images/person_1.jpg' ?>" alt="Image placeholder">
                </div>
                <div class="comment-body">
                  <h3><?= $kom['nama']; ?></h3>
                  <div class="meta"><?= $kom['tanggal_komen']; ?></div>
                  <p>
                    <?= $kom['komen']; ?>
                  </p>
                </div>
              </li>
            <?php } ?>
          </ul>
          <!-- END comment-list -->
        </div>

        <div class="comment-form-wrap pt-5">
          <h3 class="mb-5">Leave a comment</h3>
          <form method="POST" action="<?= base_url() . 'index.php/post_berita/view/' . $b['berita_id']; ?>" class="p-5 bg-light">
            <div class="form-group">
              <label for="name">Name *</label>
              <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
              <label for="email">Email *</label>
              <input type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
              <label for="message">Comment</label>
              <textarea name="komen" id="komen" cols="30" rows="6" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <label hidden for="message">ID</label>
              <input type="hidden" name="berita_id" id="berita_id" cols="30" rows="6" value='<?= $b['berita_id']; ?>' class="form-control"></input>
            </div>
            <div class="form-group">
              <label hidden for="message">Tanggal</label>
              <input type="hidden" name="tgl" id="tgl" cols="30" rows="6" value='<?= date("Y/m/d") ?>' class="form-control"></input>
            </div>
            <div class="form-group">
              <input type="submit" value="Post Comment" class="btn py-3 px-4 btn-primary">
            </div>
          </form>
        </div>


      </div> <!-- .col-md-8 -->
      <div class="col-md-4 sidebar">
        <div class="sidebar-box">
          <form action="#" class="search-form">
            <div class="form-group">
              <span class="icon fa fa-search"></span>
              <input type="text" class="form-control" placeholder="Type a keyword and hit enter">
            </div>
          </form>
        </div>
        <div class="sidebar-box">
          <div class="categories">
            <h3>Categories</h3>
            <?php
            foreach ($kategori as $kat) {
            ?>
              <li><a href="<?php echo base_url() . 'index.php/post_berita/kategori/' . $kat['id_kategori']; ?>"><?= $kat['nama_kategori']; ?>
                  <!-- <span>(12)</span> -->
                </a></li>
            <?php
            }
            ?>
          </div>
        </div>
        <div class="sidebar-box">
          <?php
          foreach ($kontributor as $kontri) {
          ?>
            <img src="<?php echo base_url() . 'assets/images/sepatu.jpg' ?>" alt="Image placeholder" class="img-fluid mb-4 rounded">
            <h3><b>About the Author</b></h3>
            <h4><?= ucwords($kontri["nama_kontributor"]); ?></h4>
            <p><?= $kontri["about_kontributor"]; ?></p>
            <p><a href="#" class="btn btn-primary btn-lg">Read More</a></p>
          <?php
          }
          ?>
        </div>

        <div class="sidebar-box">
          <h3>Tag Cloud</h3>
          <div class="tagcloud">
            <?php
            $tg = explode(',', $b['tags']);
            // print_r($tg);
            foreach ($tg as $t) {
            ?>
              <a href="#" class="tag-cloud-link"><?= $t; ?></a>
            <?php
            }
            ?>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>