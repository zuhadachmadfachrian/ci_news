   <footer class="templateux-footer bg-dark">
     <div class="container">

       <div class="row">
         <div class="col-md-12">
           <div class="row">
             <div class="col-3">
               <img src="<?php echo base_url() . 'assets/images/brand/logo.png' ?>" alt="" width="70%">
             </div>
             <div class="col-3">
               <div class="block-footer-widget">
                 <h3>Support</h3>
                 <ul class="list-unstyled">
                   <li><a href="#" style="color:#aeaeae;">FAQ</a></li>
                   <li><a href="#" style="color:#aeaeae;">Contact Us</a></li>
                   <li><a href="#" style="color:#aeaeae;">Help Desk</a></li>
                   <li><a href="#" style="color:#aeaeae;">Knowledgebase</a></li>
                 </ul>
               </div>
             </div>
             <div class="col-3">
               <div class="block-footer-widget">
                 <h3 class="text-white">About Us</h3>
                 <ul class="list-unstyled">
                   <li><a href="#" style="color:#aeaeae;">About Us</a></li>
                   <li><a href="#" style="color:#aeaeae;">Careers</a></li>
                   <li><a href="#" style="color:#aeaeae;">Terms of Service</a></li>
                   <li><a href="#" style="color:#aeaeae;">Privacy Policy</a></li>
                 </ul>
               </div>
             </div>

             <div class="col-3">
               <div class="block-footer-widget">
                 <p class="text-white">Temukan Kami</p>
                 <ul class="list-unstyled block-social">
                   <a href="https://www.instagram.com/shoesmart.id/" target="_blank"><big><i style="color: white" class="fa fa-instagram fa-lg mr-2"></i></big></a>
                   <a href="https://www.facebook.com/shoesmart.id/" target="_blank"><big><i style="color: white" class="fa fa-facebook fa-lg mr-2"></i></big></a>
                   <a href="https://www.youtube.com/channel/UC28orwEvXkTe-Pj_UZKa70w" target="_blank"><big><i style="color: white" class="fa fa-youtube fa-lg mr-2"></i></big></a>
                   <a href="https://api.whatsapp.com/send?phone=6289685555699" target="_blank"><big><i style="color: white" class="fa fa-phone fa-lg mr-2"></i></big></a>
                 </ul>
               </div>
             </div>
           </div> <!-- .row -->
         </div>
       </div>
       <div class="row pt-5 text-left">
         <div class="col-md-12 text-center text-white">
           <p>
             &copy;<script>
               document.write(new Date().getFullYear());
             </script> All rights reserved | by Shoesmart
           </p>
         </div>
       </div>
     </div>
   </footer>
   </div>

   <script src="<?php echo base_url() . 'assets/js/scripts-all.js' ?>"></script>
   <script src="<?php echo base_url() . 'assets/js/main.js' ?>"></script>
   <script src="<?php echo base_url() . 'assets/jquery/jquery-2.2.3.min.js' ?>"></script>
   <script type="text/javascript" src="<?php echo base_url() . 'assets/js/bootstrap.js' ?>"></script>

   <script src="<?php echo base_url(); ?>assets/js/foundation.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/app.js"></script>
   <!-- <script src="<?php echo base_url(); ?>assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script> -->

   </body>

   </!-->