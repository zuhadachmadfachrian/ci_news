    <div class="templateux-cover" style="background-image: url(<?php echo base_url() . 'assets/images/banner.jpg' ?>);">
      <div class="container">
        <div class="row align-items-lg-center">

          <div class="col-lg-6 order-lg-1">
            <h3 class="heading text-white" data-aos="fade-up">THE NEW</h3>
            <h3 class="heading mb-3 text-white" data-aos="fade-up">HOT ITEMS</h3>
            <p data-aos="fade-up" data-aos-delay="200"><a href="#" class="btn btn-primary py-3 px-4">Shop Now</a></p>
          </div>

        </div>
      </div>
    </div> <!-- .templateux-cover -->

    <div class="templateux-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 templateux-overlap">
            <div class="row">
              <div class="col-md-4" data-aos="fade-up" data-aos-delay="600">
                <div class="media block-icon-1 d-block text-left">
                  <div class="icon mb-3 text-center"><span class="ion-ios-lightbulb-outline"></span></div>
                  <div class="media-body text-center">
                    <h3 class="h5 mb-4">Intuitive Thinking</h3>
                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                  </div>
                </div> <!-- .block-icon-1 -->
              </div>
              <div class="col-md-4" data-aos="fade-up" data-aos-delay="700">
                <div class="media block-icon-1 d-block text-left">
                  <div class="icon mb-3 text-center"><span class="ion-ios-nutrition-outline"></span></div>
                  <div class="media-body text-center">
                    <h3 class="h5 mb-4">Orange for Carrots</h3>
                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                  </div>
                </div> <!-- .block-icon-1 -->
              </div>
              <div class="col-md-4" data-aos="fade-up" data-aos-delay="800">
                <div class="media block-icon-1 d-block text-left">
                  <div class="icon mb-3 text-center"><span class="ion-ios-infinite-outline"></span></div>
                  <div class="media-body text-center">
                    <h3 class="h5 mb-4">Infinite Posibilities</h3>
                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                  </div>
                </div> <!-- .block-icon-1 -->
              </div>

            </div>
          </div>
        </div>
      </div>
    </div> <!-- .templateux-section -->




    <div class="">
      <div class="d-flex flex-column flex-sm-row">
        <a href="#" class="block-thumbnail-1 one-third" style="background-image: url('<?php echo base_url() . 'assets/images/woman.jpg' ?>'); " data-aos="fade">
          <div class="block-thumbnail-content">
            <h2>When she reached the first</h2>
            <span class="post-meta">Design, Illustration</span>
          </div>
        </a>
        <a href="#" class="block-thumbnail-1 two-third" style="background-image: url('<?php echo base_url() . 'assets/images/men.jpg' ?>'); " data-aos="fade" data-aos-delay="100">
          <div class="block-thumbnail-content">
            <h2>Duden flows by their place</h2>
            <span class="post-meta">Design, Illustration</span>
          </div>
        </a>
      </div>
      <div class="d-flex flex-column flex-sm-row">
        <a href="#" class="block-thumbnail-1 two-third" style="background-image: url('<?php echo base_url() . 'assets/images/girl.jpg' ?>'); " data-aos="fade" data-aos-delay="200">
          <div class="block-thumbnail-content">
            <h2>Italic Mountains</h2>
            <span class="post-meta">Design, Illustration</span>
          </div>
        </a>
        <a href="#" class="block-thumbnail-1 one-third" style="background-image: url('<?php echo base_url() . 'assets/images/boy.jpg' ?>'); " data-aos="fade" data-aos-delay="300">
          <div class="block-thumbnail-content">
            <h2>Behind the word mountains</h2>
            <span class="post-meta">Design, Illustration</span>
          </div>
        </a>
      </div>
    </div> <!-- .templateux-section -->



    <div class="templateux-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-8 testimonial-wrap">
            <div class="quote">&ldquo;</div>
            <div class="owl-carousel wide-slider-testimonial">
              <div class="item">
                <blockquote class="block-testomonial">
                  <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                </blockquote>
              </div>

              <div class="item">
                <blockquote class="block-testomonial">
                  <p>&ldquo;When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.&rdquo;</p>
                </blockquote>
              </div>

              <div class="item">
                <blockquote class="block-testomonial">
                  <p>&ldquo;A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.&rdquo;</p>
                </blockquote>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- .container -->
    </div> <!-- .templateux-section -->
    <div class="templateux-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <img src="<?php echo base_url() . 'assets/images/brand/austin.png' ?>" width="15%">
            <img src="<?php echo base_url() . 'assets/images/brand/bettina.png' ?>" width="15%">
            <img src="<?php echo base_url() . 'assets/images/brand/clarette.png' ?>" width="15%">
            <img src="<?php echo base_url() . 'assets/images/brand/inside.png' ?>" width="15%">
            <img src="<?php echo base_url() . 'assets/images/brand/ghirardelli.png' ?>" width="15%">

            <img src="<?php echo base_url() . 'assets/images/brand/luis kreamer.png' ?>" width="15%">
            <img src="<?php echo base_url() . 'assets/images/brand/Bettina Collection-03.png' ?>" width="15%">
            <img src="<?php echo base_url() . 'assets/images/brand/LOGO-7-bg-white.png' ?>" width="15%">
            <img src="<?php echo base_url() . 'assets/images/brand/tony perotti.png' ?>" width="15%">
            <img src="<?php echo base_url() . 'assets/images/brand/alseno.png' ?>" width="15%">

            <img src="<?php echo base_url() . 'assets/images/brand/austin kids.png' ?>" width="15%">
            <img src="<?php echo base_url() . 'assets/images/brand/Logo-Ghirardelli-Kids.png' ?>" width="15%">
            <img src="<?php echo base_url() . 'assets/images/brand/alseno kids.png' ?>" width="15%">
          </div>
        </div>
      </div>
    </div>