<div class="templateux-cover" style="background-image: url(<?php echo base_url() . 'assets/images/banner.jpg' ?>);">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h6 data-aos="fade-up">Blog</h6>
                <h1 class="heading mb-3" data-aos="fade-up"> Thoughts and ideas shared to web community</h1>
            </div>
        </div>
    </div>
</div> <!-- .templateux-cover -->

<div class="templateux-section">
    <div class="container">
        <h5>Tags :</h5>
        <h1><strong><?= ucwords("$namaTag"); ?></strong></h1>
        <br>
        <div class="row">
            <?php
            function limit_words($string, $word_limit)
            {
                $words = explode(" ", $string);
                return implode(" ", array_splice($words, 0, $word_limit));
            }
            foreach ($tags as $tag) {
            ?>
                <div class="col-md-6 col-lg-4 mb-4">
                    <a href="<?php echo base_url() . 'index.php/post_berita/view/' . $tag["berita_id"]; ?>" class="block-thumbnail-1 one-whole show-text height-sm" style="background-image: url(<?php echo base_url() . 'assets/images/' . $tag["berita_image"]; ?>); " data-aos="fade" data-aos-delay="300">
                        <div class="block-thumbnail-content">
                            <h2><?php echo $tag['berita_judul']; ?></h2>
                            <span class="post-meta">January 20, 2019 &bullet; 3 Comments</span>
                        </div>
                    </a>
                </div>
            <?php } ?>

        </div> <!-- .row -->

        <!-- <div class="row mt-5">
          <div class="col-md-12 pt-5">
            <ul class="pagination custom-pagination">
              <li class="page-item prev"><a class="page-link" href="#"><i class="icon-keyboard_arrow_left"></i></a></li>
              <li class="page-item active"><a class="page-linkx href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item next"><a class="page-link" href="#"><i class="icon-keyboard_arrow_right"></i></a></li>
            </ul>


          </div>
        </div> -->

    </div>
</div> <!-- .templateux-section -->