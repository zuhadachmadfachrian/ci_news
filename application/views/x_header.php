<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Shoesmart</title>


  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/material-kit.css"> -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/style.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/bootstrap.css' ?>">
  <link rel="icon" type="image/png" href="<?php echo base_url() . 'assets/images/brand/sm-logo.png' ?>">
  <link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/img/favicon.ico">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css" />
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/foundation.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.css" />
  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/app.css" />	 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/w3.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/search.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/filter.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flipclock.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/prettify.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/foundation-datepicker.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slider.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/product-slider.css">

  <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/prettify.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/custom.modernizr.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/foundation-datepicker.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/flipclock.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147969022-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-147969022-1');
  </script>

  <!-- Global - Google Adsense -->

  <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <script>
    (adsbygoogle = window.adsbygoogle || []).push({
      google_ad_client: "ca-pub-7832228493615727",
      enable_page_level_ads: true
    });
  </script>
</head>

<body>

  <div class="js-animsition animsition" id="site-wrap" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">
    <header style="text-align:center;" class="templateux-navbar dark" role="banner">
      <div class="" data-aos="fade-down">
        <div class="row">
          <nav class="col-12 site-nav">
            <button class="d-block d-md-none hamburger hamburger--spin templateux-toggle templateux-toggle-light ml-auto templateux-toggle-menu" data-toggle="collapse" data-target="#mobile-menu" aria-controls="mobile-menu" aria-expanded="false" aria-label="Toggle navigation">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
            <div class="row">
              <div class="col-2">
              </div>

              <div class="col-8">
                <ul style="float:none" class="sf-menu templateux-menu d-none d-md-block">
                  <li><a href="#" class="animsition-link text-white">Pria</a></li>
                  <li><a href="#" class="animsition-link text-white">Wanita</a></li>
                  <li><a href="#" class="animsition-link text-white">New Arrivals</a></li>

                  <li class="templateux-logo"><a href="#" class="animsition-link text-white">SHOESMART</a></li>
                  <li>
                    <a href="#" class="animsition-link text-white">Promo</a>
                    <!-- <ul>
                      <li><a href="#">promo 1</a></li>
                      <li><a href="#">promo 2</a></li>
                      <li><a href="#">promo 3</a></li>
                    </ul> -->
                  </li>
                  <li><a href="<?php echo base_url() . 'Post_berita' ?>" class="animsition-link text-white">Blog</a></li>
                  <li><a href="#" class="animsition-link text-white">Contact</a></li>
                </ul>
              </div>

            </div>


          </nav>

        </div>
      </div>
  </div>
  </header>