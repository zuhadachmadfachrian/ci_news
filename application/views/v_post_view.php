<?php 
	$b=$data->row_array();
?>
	<div class="templateux-cover" style="background-image: url(<?php echo base_url().'assets/images/banner.jpg'?>);">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-8">
            <h6 data-aos="fade-up">Blog</h6>
            <h1 class="heading mb-3" data-aos="fade-up"> Thoughts and ideas shared to web community</h1>
          </div>
        </div>
      </div>
    </div>
	<div class="container">
		<div class="col-md-8 col-md-offset-2">
			<h2><?php echo $b['berita_judul'];?></h2><hr/>
			<img src="<?php echo base_url().'assets/images/'.$b['berita_image'];?>">
			<?php echo $b['berita_isi'];?>
		</div>
		
	</div>

