<?php
class Post_berita extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_berita');
		$this->load->library('upload');
		$this->load->library("form_validation");
		$this->load->library('session');
	}
	function index()
	{
		$x['data'] = $this->m_berita->get_all_berita();
		$this->load->view('x_header');
		$this->load->view('x_blog', $x);
		$this->load->view('x_footer');
	}

	function simpan_post()
	{
		$config['upload_path'] = './assets/images/'; //path folder
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

		$this->upload->initialize($config);
		if (!empty($_FILES['filefoto']['name'])) {
			if ($this->upload->do_upload('filefoto')) {
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/images/' . $gbr['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['quality'] = '60%';
				$config['width'] = 710;
				$config['height'] = 420;
				$config['new_image'] = './assets/images/' . $gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$gambar = $gbr['file_name'];
				$jdl = $this->input->post('judul');
				$berita = $this->input->post('berita');

				$this->m_berita->simpan_berita($jdl, $berita, $gambar);
				redirect('post_berita/lists');
			} else {
				redirect('c');
			}
		} else {
			redirect('post_berita');
		}
	}

	function lists()
	{
		$x['data'] = $this->m_berita->get_all_berita();
		$this->load->view('x_header');
		$this->load->view('x_blog', $x);
		$this->load->view('x_footer');
	}

	function view($kode)
	{
		$this->form_validation->set_rules("name", "name", "required");
		if ($this->form_validation->run() == FALSE) {
			$kode = $this->uri->segment(3);
			$data['data'] = $this->m_berita->getAllberitaByID($kode);
			$data['komen'] = $this->m_berita->getAllKomenByID($kode);
			$data['kategori'] = $this->m_berita->getAllKategori();
			$data['kontributor'] = $this->m_berita->getAllKontributoriByID($kode);
			$this->load->view('x_header');
			$this->load->view('x_blog-single', $data);
			$this->load->view('x_footer');
		} else {
			$this->load->model("m_berita");
			$this->m_berita->tambahKomen();
			$this->session->set_flashdata('success', 'Komen Berhasil ditambah');
			redirect("post_berita/view/$kode");
		}
	}

	function kategori($id) //cari kategori
	{
		$x['namaKat'] = $this->m_berita->GetKategori($id);
		$x['kategori'] = $this->m_berita->cariKategori($id);
		$this->load->view('x_header');
		$this->load->view('x_blogByKat', $x);
		$this->load->view('x_footer');
	}

	function cariTags($id)
	{
		$x['tags'] = $this->m_berita->getAllTagBy($id);
		$x['namaTag'] = $id;
		$this->load->view('x_header');
		$this->load->view('x_blogByTags', $x);
		$this->load->view('x_footer');
	}
}
